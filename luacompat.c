#include <lua.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <lauxlib.h>
#include <lualib.h>
#include <stdlib.h>
#include "defs.h"

int readlua(const char *filename, useropts *opts) {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    if (luaL_dofile(L, filename) != LUA_OK) {
        fprintf(stderr, "Failed to load %s: %s\n", filename, lua_tostring(L, -1));
        lua_close(L);
        return EXIT_FAILURE;
    }

    lua_getglobal(L, "useropts");
    if (!lua_istable(L, -1)) {
        fprintf(stderr, "useropts is not a table\n");
        lua_close(L);
        return EXIT_FAILURE;
    }

    lua_getfield(L, -1, "fg");
    if (lua_isinteger(L, -1)) {
        opts->fg = (int)lua_tointeger(L, -1);
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "bg");
    if (lua_isinteger(L, -1)) {
        opts->bg = (int)lua_tointeger(L, -1);
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "colors");
    if (lua_istable(L, -1)) {
        for (int i = 0; i < 16; ++i) {
            lua_pushinteger(L, i + 1);
            lua_gettable(L, -2);
            if (lua_isinteger(L, -1)) {
                opts->colors[i] = (int)lua_tointeger(L, -1);
            }
            lua_pop(L, 1);
        }
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "alpha");
    if (lua_isnumber(L, -1)) {
        opts->alpha = lua_tonumber(L, -1);
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "font");
    if (lua_isstring(L, -1)) {
        const char *font_str = lua_tostring(L, -1);
        strncpy(opts->font, font_str, sizeof(opts->font) - 1);
        opts->font[sizeof(opts->font) - 1] = '\0'; // Ensure null-termination
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "paddingx");
    if (lua_isinteger(L, -1)) {
        opts->paddingx = (int)lua_tointeger(L, -1);
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "paddingy");
    if (lua_isinteger(L, -1)) {
        opts->paddingy = (int)lua_tointeger(L, -1);
    }
    lua_pop(L, 1);

    lua_close(L);
    return EXIT_SUCCESS;
}

void createlua(const char *filename) {
    char dirpath[256];
    snprintf(dirpath, sizeof(dirpath), "%s", filename);
    char *last_slash = strrchr(dirpath, '/');
    if (last_slash != NULL) {
        *last_slash = '\0';
    }

    struct stat st = {0};
    if (stat(dirpath, &st) == -1) {
        if (mkdir(dirpath, 0777) == -1) {
            fprintf(stderr, "Error creating directory %s: %s\n", dirpath, strerror(errno));
            return;
        }
    }

    FILE *file = fopen(filename, "w");
    if (file == NULL) {
        fprintf(stderr, "Failed to open %s for writing\n", filename);
        return;
    }

    fprintf(file, "useropts = {\n");
    fprintf(file, "    fg = 0xFFFFFF,\n");
    fprintf(file, "    bg = 0x000000,\n");
    fprintf(file, "    colors = {\n");
    fprintf(file, "        0x000000,  -- Black\n");
    fprintf(file, "        0xFF0000,  -- Red\n");
    fprintf(file, "        0x00FF00,  -- Green\n");
    fprintf(file, "        0xFFFF00,  -- Yellow\n");
    fprintf(file, "        0x0000FF,  -- Blue\n");
    fprintf(file, "        0xFF00FF,  -- Magenta\n");
    fprintf(file, "        0x00FFFF,  -- Cyan\n");
    fprintf(file, "        0xFFFFFF,  -- White\n");
    fprintf(file, "        0x808080,  -- Bright Black (Gray)\n");
    fprintf(file, "        0xFF0000,  -- Bright Red\n");
    fprintf(file, "        0x00FF00,  -- Bright Green\n");
    fprintf(file, "        0xFFFF00,  -- Bright Yellow\n");
    fprintf(file, "        0x0000FF,  -- Bright Blue\n");
    fprintf(file, "        0xFF00FF,  -- Bright Magenta\n");
    fprintf(file, "        0x00FFFF,  -- Bright Cyan\n");
    fprintf(file, "        0xFFFFFF,  -- Bright White\n");
    fprintf(file, "    },\n");
    fprintf(file, "    alpha = 1.0,\n");
    fprintf(file, "    font = \"Monospace 12\",\n");
    fprintf(file, "    paddingx = 0");
    fprintf(file, "    paddingy = 0");
    fprintf(file, "}\n");

    fclose(file);
}
