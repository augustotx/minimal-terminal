#include <stdio.h>
#include <stdlib.h>
#include <vte/vte.h>
#include <getopt.h>
#include "defs.h"
#include "conf.h"

int globalfgcolor = -1, globalbgcolor = -1;
float globalalpha = -1;

static void child_ready(VteTerminal *terminal, GPid pid, GError *error, gpointer user_data){
    if (!terminal) return;
    if (pid == -1) gtk_main_quit();
}

static gboolean on_title_changed(GtkWidget *terminal, gpointer user_data){
    GtkWindow *window = user_data;
    gtk_window_set_title(window,
        vte_terminal_get_window_title(VTE_TERMINAL(terminal))?:"Minimal Terminal");
    return TRUE;
}

void showhelp(char **argv){
    printf("%s    The Minimal Terminal\n", argv[0]);
    printf("-f    --foreground-color      define a foreground color for the terminal to use\n");
    printf("-b    --background-color      define a background color for the terminal to use\n");
    printf("-a    --alpha                 define an alpha/transparency value for the terminal to use\n");
    printf("-h    --help                  show this message\n");
}

int main(int argc, char *argv[])
{

    static struct option long_options[] = {
        {"help",             no_argument,       0, 'h'},
        {"foreground-color", required_argument, 0, 'f'},
        {"background-color", required_argument, 0, 'b'},
        {"alpha",            required_argument, 0, 'a'},
        {0,                  0,                 0,   0 }
    };

    int opt;
    while ((opt = getopt_long(argc, argv, "hf:b:a:", long_options, NULL)) != -1){
        switch (opt) {
            case 'h':
                showhelp(argv);
                return EXIT_SUCCESS;
            case 'f':
                globalfgcolor = colorfromcli(optarg);
                break;
            case 'b':
                globalbgcolor = colorfromcli(optarg);
                break;
            case 'a':
                globalalpha = atof(optarg);
                if(globalalpha < 0 || globalalpha > 1)
                {
                    eprint("Invalid alpha value, must be between 0 and 1");
                    return EXIT_FAILURE;
                }
                break;
            case '?':
                showhelp(argv);
                return EXIT_SUCCESS;
            default:
                abort();

        }
    }

    GtkWidget *window, *terminal;

    // window
    gtk_init(&argc, &argv);
    terminal = vte_terminal_new();
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Minimal Terminal");
    gtk_widget_set_app_paintable(window, TRUE);
    gtk_widget_realize(window);

    // get shell
    gchar **envp = g_get_environ();
    gchar **command = (gchar *[]){g_strdup(g_environ_getenv(envp, "SHELL")), NULL };
    g_strfreev(envp);
    vte_terminal_spawn_async(VTE_TERMINAL(terminal),
        VTE_PTY_DEFAULT,
        NULL,         // working directory
        command,      // command
        NULL,         // environment
        0,            // spawn flags
        NULL, NULL,   // child setup
        NULL,         // child pid
        -1,           // timeout
        NULL,         // cancellable
        child_ready,  // callback
        NULL);        // user_data

    // signals
    g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
    g_signal_connect(terminal, "child-exited", gtk_main_quit, NULL);
    g_signal_connect(terminal, "window-title-changed", G_CALLBACK(on_title_changed), GTK_WINDOW(window));
    g_signal_connect(terminal, "key-press-event", G_CALLBACK(on_key_press), NULL);

    // set terminal user config
    useropts opts = readconfig();

    gtk_widget_set_margin_start(terminal, opts.paddingx);
    gtk_widget_set_margin_end(terminal, opts.paddingx);
    gtk_widget_set_margin_top(terminal, opts.paddingy);
    gtk_widget_set_margin_bottom(terminal, opts.paddingy);

    user_config(terminal, window, opts);

    gtk_container_add(GTK_CONTAINER(window), terminal);
    gtk_widget_show_all(window);

    gtk_window_set_decorated(GTK_WINDOW(window),FALSE);

    gtk_main();
}
