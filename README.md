# Minimal Terminal

Minimal Terminal (minterm) is a barebones terminal created using VTE for the window and customization and Lua for its configuration.

### Dependencies
Void Linux: `sudo xbps-install lua-devel vte3-devel`

If I'm not mistaken, on Arch you can just install lua and vte and it should work

### Building
```bash
git clone https://gitlab.com/augustotx/minimal-terminal.git
cd minimal-terminal
make
```

You can install it with:
```bash
sudo make install # requires sudo because it installs the binary to /bin
```

### Issues?
Post any issues here on the repo, I'll check them out as soon as I can!
