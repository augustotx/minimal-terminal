#include <vte/vte.h>
#include <string.h>
#include <stdio.h>
#include <pango/pangocairo.h>
#include <stdlib.h>
#include "defs.h"
#include "luacompat.h"

extern int globalfgcolor, globalbgcolor;
extern float globalalpha;

useropts readconfig(){
    useropts opts;
    opts.fg = 0xffffff;
    opts.bg = 0x000000;
    opts.alpha = 1.0;
    opts.paddingx = 0;
    opts.paddingy = 0;
    strcpy(opts.font,"Monospace 12");
    int defaultcolors[16] = {
        0x000000, // Black
        0xFF0000, // Red
        0x00FF00, // Green
        0xFFFF00, // Yellow
        0x0000FF, // Blue
        0xFF00FF, // Magenta
        0x00FFFF, // Cyan
        0xFFFFFF, // White
        0x808080, // Bright Black (Gray)
        0xFF0000, // Bright Red
        0x00FF00, // Bright Green
        0xFFFF00, // Bright Yellow
        0x0000FF, // Bright Blue
        0xFF00FF, // Bright Magenta
        0x00FFFF, // Bright Cyan
        0xFFFFFF  // Bright White
    };
    int i;
    for(i = 0; i < 16; i++){
        opts.colors[i] = defaultcolors[i];
    }
    char *path = malloc(255 * sizeof(char));
    sprintf(path, "%s/.config/minterm/config.lua",getenv("HOME"));
    if(readlua(path, &opts) == EXIT_FAILURE){
            createlua(path);
    }
    free(path);
    if(globalfgcolor != -1)
    {
        opts.fg = globalfgcolor;
    }
    if(globalbgcolor != -1)
    {
        opts.bg = globalbgcolor;
    }
    if(globalalpha != -1.0)
    {
        opts.alpha = globalalpha;
    }
    return opts;
}

void set_terminal_font(VteTerminal *terminal, const char *font_string) {
    PangoFontDescription *font_desc = pango_font_description_from_string(font_string);
    vte_terminal_set_font(terminal, font_desc);
    pango_font_description_free(font_desc);
}

void user_config(GtkWidget *terminal, GtkWidget *window, useropts opts){
    GdkRGBA fg_color = CLR_GDK(opts.fg);
    GdkRGBA bg_color = CLR_GDK(opts.bg);
    bg_color.alpha = opts.alpha;
    GdkRGBA colors[16];
    for (int i = 0; i < 16; ++i) {
        colors[i] = CLR_GDK(opts.colors[i]);
    }

    vte_terminal_set_colors(VTE_TERMINAL(terminal), &fg_color, &bg_color, colors, 16);

    set_terminal_font(VTE_TERMINAL(terminal), opts.font);

    vte_terminal_set_scroll_on_output(VTE_TERMINAL(terminal), FALSE);
    vte_terminal_set_scroll_on_keystroke(VTE_TERMINAL(terminal), TRUE);
    vte_terminal_set_mouse_autohide(VTE_TERMINAL(terminal), TRUE);

    // this line is needed due to how the window's opacity conflicts with VTE (terminal backend) if this line is gone, the terminal will flicker like crazy
    gdk_window_set_background_rgba(gtk_widget_get_window(window), &bg_color);
}

