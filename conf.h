#include <vte/vte.h>
#include "defs.h"

void eprint(const char *format);
useropts readconfig();
void set_terminal_font(VteTerminal *terminal, const char *font_string);
void user_config(GtkWidget *terminal, GtkWidget *window, useropts opts);
int colorfromcli(char *hex);
