#include <vte/vte.h>

#ifndef DEFS_H
#define DEFS_H

#define CLR_R(x)   (((x) & 0xff0000) >> 16)
#define CLR_G(x)   (((x) & 0x00ff00) >>  8)
#define CLR_B(x)   (((x) & 0x0000ff) >>  0)
#define CLR_16(x)  ((double)(x) / 0xff)
#define CLR_GDK(x) (const GdkRGBA){ .red = CLR_16(CLR_R(x)), .green = CLR_16(CLR_G(x)), .blue = CLR_16(CLR_B(x)), .alpha = 1 }


typedef struct {
    int fg, bg;
    int colors[16];
    double alpha;
    char font[255];
    int paddingx, paddingy;
} useropts;

gboolean on_key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data);

#endif
