
build:
	gcc -O2 -Wall -o minterm minterm.c defs.c luacompat.c conf.c `pkg-config --libs --cflags vte-2.91 lua-5.4 pangocairo`
install: build
	cp ./minterm /bin/
