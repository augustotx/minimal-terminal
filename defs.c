#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <vte/vte.h>
#include "defs.h"
#include "conf.h"

void eprint(const char *format)
{
    fprintf(stderr, "%s", format);
}

void on_clipboard_text_received(GtkClipboard *clipboard, const gchar *text, gpointer user_data) {
    if (text != NULL) {
        vte_terminal_feed_child(VTE_TERMINAL(user_data), text, strlen(text));
    }
}

gboolean on_key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data) {
    switch (event->keyval) {
        case GDK_KEY_C:
            if ((event->state & GDK_CONTROL_MASK)) {
                if(event->state & GDK_SHIFT_MASK){
                    // Ctrl+Shift+C (Copy)
                    vte_terminal_copy_clipboard_format(VTE_TERMINAL(widget), VTE_FORMAT_TEXT);
                    return TRUE;
                } else {
                    // Ctrl+C (Interrupt)
                    vte_terminal_feed_child(VTE_TERMINAL(widget), "\x03", 1);
                    return TRUE;
                }
            }
            break;
        case GDK_KEY_V:
            // raw paste
            if((event->state & GDK_CONTROL_MASK) && (event->state & GDK_SHIFT_MASK)){
                GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
                gtk_clipboard_request_text(clipboard, on_clipboard_text_received, widget);
                return TRUE;
            }
            break;
        case GDK_KEY_D:
            if (event->state & GDK_CONTROL_MASK) {
                // Ctrl+D (EOF)
                vte_terminal_feed_child(VTE_TERMINAL(widget), "\x04", 1);
                return TRUE;
            }
            break;
        case GDK_KEY_Z:
            if (event->state & GDK_CONTROL_MASK) {
                // Ctrl+Z (Suspend)
                vte_terminal_feed_child(VTE_TERMINAL(widget), "\x1A", 1);
                return TRUE;
            }
            break;
        default:
            break;
    }
    return FALSE;
}

int colorfromcli(char *hex)
{
    if(strlen(hex) != 6)
    {
        eprint("Invalid hex color code.\n");
        exit(EXIT_FAILURE);
    }
    int i;
    for(i = 0; i < 6; i++){
        if(!isxdigit(hex[i])){
            eprint("Invalid hex color code.\n");
            exit(EXIT_FAILURE);
        }
    }

    int color = (int)strtol(hex, NULL, 16);
    return color;
}
